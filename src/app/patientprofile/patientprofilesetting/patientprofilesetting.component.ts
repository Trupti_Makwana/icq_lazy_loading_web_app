import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import $ from "jquery";

@Component({
  selector: 'app-patientprofilesetting',
  templateUrl: './patientprofilesetting.component.html',
  styleUrls: ['./patientprofilesetting.component.css']
})
export class PatientprofilesettingComponent implements OnInit {
  listOfFiles1: any = [{}];
  patientprofileForm: FormGroup;
  Display_Image: any;
  fileExtension: any;
  ispdfFile: boolean = false;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.listOfFiles1.pop();
    this.buildForm();
  }

  /*- Submit Data -*/
  onSubmit() {
    if (this.patientprofileForm.invalid) {
      return;
    }
  }
  /*- Required Fields -*/
  get f() { return this.patientprofileForm.controls; }
  buildForm() {
    this.patientprofileForm = this.formBuilder.group({
      FirstName: ['', Validators.required],
      LastName: ['', Validators.required],
      dateofbirth: ['', Validators.required],
      EmailAddress: ['', [Validators.required, Validators.pattern(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      City: ['', Validators.required],
      PhoneNumber: ['', [Validators.required, Validators.minLength(10)]],
      ZipCode: ['', Validators.required],
      ProfilePic: ['', Validators.required],
    });
  }

  /*- Remove Selected File -*/
  removeSelectedFile(index) {
    var removeIndex = this.listOfFiles1.map(item => item.FileName).indexOf(index);
    this.listOfFiles1.splice(removeIndex, 1);
    if (index == "ProfilePicPath") {
      this.Display_Image = null;
      this.patientprofileForm.controls["ProfilePic"].reset();
    }
  }

  /*- View Image And PDF -*/
  onSelectImagePathFile(event, strName) {
    if (strName == "ProfilePicPath") {
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]); // read file as data url
        reader.onload = (event: any) => { // called once readAsDataURL is completed
          this.Display_Image = event.target.result;
        }
      }
    }
  }

  /*- Only Number Enter -*/
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}
