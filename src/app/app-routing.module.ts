import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ForServiceProviderComponent } from './for-service-provider/for-service-provider.component';
import { ForServiceUserComponent } from './for-service-user/for-service-user.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { CookiesPolicyComponent } from './cookies-policy/cookies-policy.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { NewsLatterComponent } from './news-latter/news-latter.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'about', component: AboutComponent },
  { path: 'news-latter', component: NewsLatterComponent },
  { path: 'for-service-provider', component: ForServiceProviderComponent },
  { path: 'for-service-user', component: ForServiceUserComponent },
  { path: 'contact-us', component: ContactUsComponent },
  { path: 'terms-condition', component: TermsConditionsComponent },
  { path: 'privacy-policy', component: PrivacyPolicyComponent },
  { path: 'cookies-policy', component: CookiesPolicyComponent },
  { path: 'book-home-visit', loadChildren: () => import('./bookhomevisit/bookhomevisit.module').then(m => m.BookhomevisitModule) },
  { path: 'service-provider', loadChildren: () => import('./becomeserviceprovider/becomeserviceprovider.module').then(m => m.BecomeserviceproviderModule) },
  { path: 'account', loadChildren: () => import('./account/account.module').then(m => m.AccountModule) },
  { path: 'patient', loadChildren: () => import('./patientprofile/patientprofile.module').then(m => m.PatientprofileModule) },
  { path: 'service-provider-profile', loadChildren: () => import('./doctorprofile/doctorprofile.module').then(m => m.DoctorprofileModule) },
  { path: '**', component: PagenotfoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
