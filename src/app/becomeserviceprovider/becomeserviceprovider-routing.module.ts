import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BecomeserviceproviderComponent } from './becomeserviceprovider.component';
import { DoctorregisterstepsComponent } from './doctorregistersteps/doctorregistersteps.component';




const routes: Routes = [
  {
    path: '', component: BecomeserviceproviderComponent,
    children: [
      { path: 'register', component: DoctorregisterstepsComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BecomeserviceproviderRoutingModule { }
