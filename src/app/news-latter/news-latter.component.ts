import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-news-latter',
  templateUrl: './news-latter.component.html',
  styleUrls: ['./news-latter.component.css']
})
export class NewsLatterComponent implements OnInit {
  IsOpenModel: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }
  openModel() {
    this.IsOpenModel = true;
  }

  closeModel() {
    this.IsOpenModel = false;
  }
}
