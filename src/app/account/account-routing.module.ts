import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountComponent } from './account.component';
import { PatientloginComponent } from './patientlogin/patientlogin.component';
import { DoctorloginComponent } from './doctorlogin/doctorlogin.component';
import { PatientotpComponent } from './patientotp/patientotp.component';


const routes: Routes = [
  {
    path: '', component: AccountComponent,
    children: [
      { path: 'patient-login', component: PatientloginComponent },
      { path: 'service-provider-login', component: DoctorloginComponent },
      { path: 'patient-otp', component: PatientotpComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
