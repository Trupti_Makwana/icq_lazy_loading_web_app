import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-doctorlogin',
  templateUrl: './doctorlogin.component.html',
  styleUrls: ['./doctorlogin.component.css']
})
export class DoctorloginComponent implements OnInit {
  doctorForm: FormGroup;

  constructor(private formBuilder: FormBuilder, public router: Router) { }

  ngOnInit(): void {
    this.buildForm();
  }

  onSubmit() {
    // stop here if form is invalid
    if (this.doctorForm.invalid) {
      return;
    }else{
      this.router.navigate(["service-provider-profile/service-provider-profilesetting"]);
    }
  }

  get f() { return this.doctorForm.controls; }
  buildForm() {
    this.doctorForm = this.formBuilder.group({
      EmailAddress: ['', [Validators.required, Validators.pattern(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      Password: ['', [Validators.required, Validators.minLength(6)]],
    }
    );
  }
}
