import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DoctorprofileComponent } from './doctorprofile.component';
import { DoctordashboardComponent } from './doctordashboard/doctordashboard.component';
import { DoctorprofilesettingComponent } from './doctorprofilesetting/doctorprofilesetting.component';
import { DoctorreviewComponent } from './doctorreview/doctorreview.component';
import { DoctorhomevisitComponent } from './doctorhomevisit/doctorhomevisit.component';
import { DoctorchangepasswordComponent } from './doctorchangepassword/doctorchangepassword.component';



const routes: Routes = [
  {
    path: '', component: DoctorprofileComponent,
    children: [
      { path: 'service-provider-dashboard', component: DoctordashboardComponent },
      { path: 'service-provider-profilesetting', component: DoctorprofilesettingComponent },
      { path: 'service-provider-review', component: DoctorreviewComponent },
      { path: 'service-provider-home-visit', component: DoctorhomevisitComponent },
      { path: 'service-provider-change-password', component: DoctorchangepasswordComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DoctorprofileRoutingModule { }
