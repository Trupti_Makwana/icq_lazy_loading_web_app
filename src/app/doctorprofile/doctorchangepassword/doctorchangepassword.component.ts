import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { MustMatch } from 'src/app/shared/helpers/MustMatch';

@Component({
  selector: 'app-doctorchangepassword',
  templateUrl: './doctorchangepassword.component.html',
  styleUrls: ['./doctorchangepassword.component.css']
})

export class DoctorchangepasswordComponent implements OnInit {
  ResetForm: FormGroup;
  submitted = false;
  fieldTextTypeOld: boolean;
  fieldTextTypeNew: boolean;
  fieldTextTypeNewConfirm: boolean;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.buildForm();
  }


  get f() { return this.ResetForm.controls; }
  buildForm() {
    this.ResetForm = this.formBuilder.group({
      OldPassword: ['', Validators.required],
      NewPassword: ['', Validators.required],
      ConfirmPassword: ['', Validators.required]
    }, {
      validator: MustMatch('NewPassword', 'ConfirmPassword')
    });
  }

  // Show Password
  toggleFieldTextTypeOld() {
    this.fieldTextTypeOld = !this.fieldTextTypeOld;
  }
  toggleFieldTextTypeNew() {
    this.fieldTextTypeNew = !this.fieldTextTypeNew;
  }
  toggleFieldTextTypeConfirm() {
    this.fieldTextTypeNewConfirm = !this.fieldTextTypeNewConfirm;
  }


  // Change Password Method
  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.ResetForm.invalid) {
      return;
    }
  }
}