import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BecomeserviceproviderRoutingModule } from './becomeserviceprovider-routing.module';
import { BecomeserviceproviderComponent } from './becomeserviceprovider.component';
import { DoctorregisterstepsComponent } from './doctorregistersteps/doctorregistersteps.component';
import { GooglePlaceModule } from 'ngx-google-places-autocomplete';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [BecomeserviceproviderComponent, DoctorregisterstepsComponent],
  imports: [
    CommonModule,
    BecomeserviceproviderRoutingModule,
    GooglePlaceModule,
    ReactiveFormsModule,
    FormsModule,
  ]
})
export class BecomeserviceproviderModule { }
