import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-patientlogin',
  templateUrl: './patientlogin.component.html',
  styleUrls: ['./patientlogin.component.css']
})
export class PatientloginComponent implements OnInit {
  patientForm: FormGroup;

  constructor(private formBuilder: FormBuilder, public router: Router) { }

  ngOnInit(): void {
    this.buildForm();
  }
  onSubmit() {
    // stop here if form is invalid
    if (this.patientForm.invalid) {
      return;
    } else {
      this.router.navigate(["account/patient-otp"]);
    }
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  get f() { return this.patientForm.controls; }
  buildForm() {
    this.patientForm = this.formBuilder.group({
      PhoneNumber: ['', [Validators.required, Validators.minLength(10)]],
    }
    );
  }
}
