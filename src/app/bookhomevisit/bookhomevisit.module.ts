import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { BookhomevisitRoutingModule } from './bookhomevisit-routing.module';
import { BookhomevisitComponent } from './bookhomevisit.component';
import { BookhomevisitdoctorlistComponent } from './bookhomevisitdoctorlist/bookhomevisitdoctorlist.component';
import { BookhomevisitdoctorviewprofileComponent } from './bookhomevisitdoctorviewprofile/bookhomevisitdoctorviewprofile.component';
import { BookhomevisitdoctorbookappointmentComponent } from './bookhomevisitdoctorbookappointment/bookhomevisitdoctorbookappointment.component';
import { BookhomevisitproceedtopayComponent } from './bookhomevisitproceedtopay/bookhomevisitproceedtopay.component';
import { BookhomevisitbookedsuccessfullyComponent } from './bookhomevisitbookedsuccessfully/bookhomevisitbookedsuccessfully.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgxPayPalModule } from 'ngx-paypal';


@NgModule({
  declarations: [BookhomevisitComponent, BookhomevisitdoctorlistComponent, BookhomevisitdoctorviewprofileComponent, BookhomevisitdoctorbookappointmentComponent, BookhomevisitproceedtopayComponent, BookhomevisitbookedsuccessfullyComponent],
  imports: [
    CommonModule,
    BookhomevisitRoutingModule,
    FormsModule, ReactiveFormsModule,
    NgxPayPalModule,
  ]
})
export class BookhomevisitModule { }
