import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-doctorregistersteps',
  templateUrl: './doctorregistersteps.component.html',
  styleUrls: ['./doctorregistersteps.component.css']
})
export class DoctorregisterstepsComponent implements OnInit {

  serviceProvideForm: FormGroup;
  submitted = false;
  listOfFiles1: any = [{}];
  // *Check File Extentation Variable*
  fileExtension: any;
  Display_Profile: any;
  Display_CV: any;
  Display_PassPort: any;
  Display_DrivingLicence: any;
  Display_ProofAddress: any;
  Display_DBSCertificate: any;

  // *Check File Extentation Type*
  fileExtProfilePic: boolean = false;
  FileExtCV: boolean = false;
  FileExtPassPort: boolean = false;
  FileExtDrivingLicence: boolean = false;
  FileExtProofAddress: boolean = false;
  FileExtDBSCertificate: boolean = false;

  // *Next Hide Show Button*
  show: boolean = false;
  buttonshow: boolean = true;
  barwidth: number = 10;
  step1: boolean = true;
  step2: boolean = false;
  step3: boolean = false;
  step4: boolean = false;
  step5: boolean = false;
  step6: boolean = false;

  // *not a valid 10 year of old*
  yearMS = 365 * (1000 * 60 * 60 * 24); // 365 days
  now = new Date().getTime();
  maxDobMS = this.now - (18 * this.yearMS);
  minDobMS = this.now - (122.5 * this.yearMS);
  maxDobString = new Date(this.maxDobMS).toISOString().slice(0, 10);
  minDobString = new Date(this.minDobMS).toISOString().slice(0, 10);

  options = {
    componentRestrictions: { country: 'UK' }
  }

  // PassWord Show Variable
  fieldTextTypepassWord: boolean;
  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.listOfFiles1.pop();
    this.buildForm();
  }
  // *Reset Upload File Method*
  resetProfile() {
    this.Display_Profile = null;
  }
  resetAllUploadFiles() {
    this.Display_CV = null;
    this.Display_PassPort = null;
    this.Display_DrivingLicence = null;
    this.Display_ProofAddress = null;
    this.Display_DBSCertificate = null;

  }
  // *Save Service Provider Register Detail*
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.serviceProvideForm.invalid) {
      return;
    }
  }
  // * Date Of Birth 18 Year old validation*
  get DOB() { return this.serviceProvideForm.controls.DOB; }
  get f() { return this.serviceProvideForm.controls; }

  getErrorDOB() {
    if (this.DOB.touched && this.DOB.hasError('required')) {
      return 'Date of birth is required';
    }
    var today = new Date(this.DOB.value);
    var birthDate = new Date();
    var age = birthDate.getFullYear() - today.getFullYear();
    if (age >= 18) {
    }
    else {
      return 'Sorry, all service provide must be at least 18 years of age.';
    }
  }

  buildForm() {
    this.serviceProvideForm = this.formBuilder.group({
      PlaceName: ['', Validators.required],
      CategoryName: ['', Validators.required],
      FristName: ['', Validators.required],
      LastName: ['', Validators.required],
      PhoneNumber: ['', [Validators.required, Validators.minLength(10)]],
      EmailAddress: ['', [Validators.required, Validators.pattern(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      DOB: ['', Validators.required],
      Password: ['', [Validators.required, Validators.minLength(6)]],
      Gender: ['', Validators.required],
      DBS: ['', Validators.required],
      AboutMe: ['', Validators.required],
      BasicPrice: ['', Validators.required],
      CV: ['', Validators.required],
      PassPort: ['', Validators.required],
      DrivingLicence: ['', Validators.required],
      ProofofAddress: ['', Validators.required],
      DBSCertificate: ['', Validators.required],
      AccountHolderName: ['', Validators.required],
      AccountNo: ['', Validators.required],
      BankName: ['', Validators.required],
      SortCode: ['', Validators.required],
    }
    );
  }

  // * Next Step Desable Method*
  NestStep1() {
    if (this.serviceProvideForm.controls["PlaceName"].valid && this.serviceProvideForm.controls["CategoryName"].valid) {
      return false
    }
    else {
      return true
    }
  }
  NestStep2() {
    if (this.serviceProvideForm.controls["FristName"].valid && this.serviceProvideForm.controls["LastName"].valid
      && this.serviceProvideForm.controls["PhoneNumber"].valid && this.serviceProvideForm.controls["EmailAddress"].valid
      && this.serviceProvideForm.controls["DOB"].valid && this.serviceProvideForm.controls["Password"].valid
      && this.serviceProvideForm.controls["Gender"].valid && this.serviceProvideForm.controls["DBS"].valid) {
      return false
    }
    else {
      return true
    }
  }
  NestStep3() {
    if (this.serviceProvideForm.controls["AboutMe"].valid && this.serviceProvideForm.controls["CV"].valid
      && this.serviceProvideForm.controls["PassPort"].valid && this.serviceProvideForm.controls["DrivingLicence"].valid
      && this.serviceProvideForm.controls["ProofofAddress"].valid && this.serviceProvideForm.controls["DBSCertificate"].valid) {
      return false
    }
    else {
      return true
    }
  }
  NestStep4() {
    if (this.serviceProvideForm.controls["BasicPrice"].valid) {
      return false
    }
    else {
      return true
    }
  }

  /*- checks File Extentation -*/
  onSelectImagePathFile(event, strName) {
    if (strName == "DoctorProfilePic") {
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]); // read file as data url
        reader.onload = (event: any) => { // called once readAsDataURL is completed
          this.Display_Profile = event.target.result;
        }
      }
    }
    if (strName == "CV") {
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]); // read file as data url
        reader.onload = (event: any) => { // called once readAsDataURL is completed
          this.Display_CV = event.target.result;
        }
      }
      var file = event.target.files[0];
      this.Display_CV = file.name;
      this.fileExtension = this.Display_CV.split('.').pop();
      if (this.fileExtension == "pdf") {
        this.FileExtCV = true;
      }
      else {
        this.FileExtCV = false;
      }
    }
    if (strName == "PassPort") {
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]); // read file as data url
        reader.onload = (event: any) => { // called once readAsDataURL is completed
          this.Display_PassPort = event.target.result;
        }
      }
      var file = event.target.files[0];
      this.Display_PassPort = file.name;
      this.fileExtension = this.Display_PassPort.split('.').pop();
      if (this.fileExtension == "pdf") {
        this.FileExtPassPort = true;
      }
      else {
        this.FileExtPassPort = false;
      }
    }
    if (strName == "DrivingLicence") {
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]); // read file as data url
        reader.onload = (event: any) => { // called once readAsDataURL is completed
          this.Display_DrivingLicence = event.target.result;
        }
      }
      var file = event.target.files[0];
      this.Display_DrivingLicence = file.name;
      this.fileExtension = this.Display_DrivingLicence.split('.').pop();
      if (this.fileExtension == "pdf") {
        this.FileExtDrivingLicence = true;
      }
      else {
        this.FileExtDrivingLicence = false;
      }
    }
    if (strName == "ProofofAddress") {
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]); // read file as data url
        reader.onload = (event: any) => { // called once readAsDataURL is completed
          this.Display_ProofAddress = event.target.result;
        }
      }
      var file = event.target.files[0];
      this.Display_ProofAddress = file.name;
      this.fileExtension = this.Display_ProofAddress.split('.').pop();
      if (this.fileExtension == "pdf") {
        this.FileExtProofAddress = true;
      }
      else {
        this.FileExtProofAddress = false;
      }
    }
    if (strName == "DBSCertificate") {
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]); // read file as data url
        reader.onload = (event: any) => { // called once readAsDataURL is completed
          this.Display_DBSCertificate = event.target.result;
        }
      }
      var file = event.target.files[0];
      this.Display_DBSCertificate = file.name;
      this.fileExtension = this.Display_DBSCertificate.split('.').pop();
      if (this.fileExtension == "pdf") {
        this.FileExtDBSCertificate = true;
      }
      else {
        this.FileExtDBSCertificate = false;
      }
    }
  }

  /*- checks if word exists in array -*/
  isInArray(array, word) {
    return array.indexOf(word.toLowerCase()) > -1;
  }
  removeSelectedFile(index) {
    var removeIndex = this.listOfFiles1.map(item => item.FileName).indexOf(index);
    this.listOfFiles1.splice(removeIndex, 1);
    if (index == "DoctorProfilePic") {
      this.Display_Profile = null;
    }
    if (index == "CV") {
      this.Display_CV = null;
      this.serviceProvideForm.controls["CV"].reset();
    }
    if (index == "PassPort") {
      this.Display_PassPort = null;
      this.serviceProvideForm.controls["PassPort"].reset();
    }
    if (index == "DrivingLicence") {
      this.Display_DrivingLicence = null;
      this.serviceProvideForm.controls["DrivingLicence"].reset();
    }
    if (index == "ProofofAddress") {
      this.Display_ProofAddress = null;
      this.serviceProvideForm.controls["ProofofAddress"].reset();
    }
    if (index == "DBSCertificate") {
      this.Display_DBSCertificate = null;
      this.serviceProvideForm.controls["DBSCertificate"].reset();
    }
  }
  next(step: any) {
    if (step == 1) {
      this.step1 = false;
      this.step2 = false;
      this.step3 = false;
      this.step4 = false;
      this.step5 = false;
      this.step6 = false;
    }
    if (step == 2) {
      this.step1 = false;
      this.step2 = true;
      this.step3 = false;
      this.step4 = false;
      this.step5 = false;
      this.step6 = false;
    }
    if (step == 3) {
      this.step1 = false;
      this.step2 = false;
      this.step3 = true;
      this.step4 = false;
      this.step5 = false;
      this.step6 = false;
    }
    if (step == 4) {
      this.step1 = false;
      this.step2 = false;
      this.step3 = false;
      this.step4 = true;
      this.step5 = false;
      this.step6 = false;
    }
    if (step == 5) {
      this.step1 = false;
      this.step2 = false;
      this.step3 = false;
      this.step4 = false;
      this.step5 = true;
      this.step6 = false;
    }
    this.show = true;
    this.barwidth = 20;
    this.buttonshow = false;
  }
  previous(step: any) {
    if (step == 1) {
      this.step1 = true;
      this.step2 = false;
      this.step3 = false;
      this.step4 = false;
      this.step5 = false;
      this.step6 = false;
    }
    if (step == 2) {
      this.step1 = false;
      this.step2 = true;
      this.step3 = false;
      this.step4 = false;
      this.step5 = false;
      this.step6 = false;
    }
    if (step == 3) {
      this.step1 = false;
      this.step2 = false;
      this.step3 = true;
      this.step4 = false;
      this.step5 = false;
      this.step6 = false;
    }
    if (step == 4) {
      this.step1 = false;
      this.step2 = false;
      this.step3 = false;
      this.step4 = true;
      this.step5 = false;
      this.step6 = false;
    }

    this.show = false;
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  toggleFieldTextTypePassWord() {
    this.fieldTextTypepassWord = !this.fieldTextTypepassWord;
  }

}
