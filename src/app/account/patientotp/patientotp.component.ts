import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-patientotp',
  templateUrl: './patientotp.component.html',
  styleUrls: ['./patientotp.component.css']
})
export class PatientotpComponent implements OnInit {
  patientOTPForm: FormGroup;

  constructor(private formBuilder: FormBuilder, public router: Router) { }

  ngOnInit(): void {
    this.buildForm();
  }

  onSubmit() {
    // stop here if form is invalid
    if (this.patientOTPForm.invalid) {
      return;
    }
  }
  verifyOTP() {
    this.router.navigate(["patient/patient-profile-setting"]);
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
  get f() { return this.patientOTPForm.controls; }
  buildForm() {
    this.patientOTPForm = this.formBuilder.group({
      OTPNumber: ['', Validators.required],
    }
    );
  }

}
