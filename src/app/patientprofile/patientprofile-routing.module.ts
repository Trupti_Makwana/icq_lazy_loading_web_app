import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientprofileComponent } from './patientprofile.component';
import { PatientdashboardComponent } from './patientdashboard/patientdashboard.component';
import { PatientprofilesettingComponent } from './patientprofilesetting/patientprofilesetting.component';



const routes: Routes = [
  {
    path: '', component: PatientprofileComponent,
    children: [
      { path: 'patient-dashboard', component: PatientdashboardComponent },
      { path: 'patient-profile-setting', component: PatientprofilesettingComponent },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientprofileRoutingModule { }
