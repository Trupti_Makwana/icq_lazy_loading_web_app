import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-doctorprofilesetting',
  templateUrl: './doctorprofilesetting.component.html',
  styleUrls: ['./doctorprofilesetting.component.css']
})
export class DoctorprofilesettingComponent implements OnInit {
  listOfFiles1: any = [{}];
  doctorprofileform: FormGroup;
  submitted = false;
  // *Check File Extentation Variable*
  fileExtension: any;
  Display_Profile: any;
  Display_CV: any;
  Display_PassPort: any;
  Display_DrivingLicence: any;
  Display_ProofAddress: any;
  Display_DBSCertificate: any;

  // *Check File Extentation Type*
  fileExtProfilePic: boolean = false;
  FileExtCV: boolean = false;
  FileExtPassPort: boolean = false;
  FileExtDrivingLicence: boolean = false;
  FileExtProofAddress: boolean = false;
  FileExtDBSCertificate: boolean = false;

  options = {
    componentRestrictions: {
      country: ["UK"]
    }
  }

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.listOfFiles1.pop();
    this.buildForm();
  }

  onSubmit() {
    this.submitted = true;
    if (this.doctorprofileform.invalid) {
      return;
    }
  }
  removeSelectedFile(index) {
    var removeIndex = this.listOfFiles1.map(item => item.FileName).indexOf(index);
    this.listOfFiles1.splice(removeIndex, 1);
    if (index == "DoctorProfilePic") {
      this.Display_Profile = null;
    }
    if (index == "CV") {
      this.Display_CV = null;
      this.doctorprofileform.controls["CV"].reset();
    }
    if (index == "PassPort") {
      this.Display_PassPort = null;
      this.doctorprofileform.controls["PassPort"].reset();
    }
    if (index == "DrivingLicence") {
      this.Display_DrivingLicence = null;
      this.doctorprofileform.controls["DrivingLicence"].reset();
    }
    if (index == "ProofofAddress") {
      this.Display_ProofAddress = null;
      this.doctorprofileform.controls["ProofofAddress"].reset();
    }
    if (index == "DBSCertificate") {
      this.Display_DBSCertificate = null;
      this.doctorprofileform.controls["DBSCertificate"].reset();
    }

  }
  /*- checks File Extentation -*/
  onSelectImagePathFile(event, strName) {
    if (strName == "DoctorProfilePic") {
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]); // read file as data url
        reader.onload = (event: any) => { // called once readAsDataURL is completed
          this.Display_Profile = event.target.result;
        }
      }
    }
    if (strName == "CV") {
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]); // read file as data url
        reader.onload = (event: any) => { // called once readAsDataURL is completed
          this.Display_CV = event.target.result;
        }
      }
      var file = event.target.files[0];
      this.Display_CV = file.name;
      this.fileExtension = this.Display_CV.split('.').pop();
      if (this.fileExtension == "pdf") {
        this.FileExtCV = true;
      }
      else {
        this.FileExtCV = false;
      }
    }
    if (strName == "PassPort") {
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]); // read file as data url
        reader.onload = (event: any) => { // called once readAsDataURL is completed
          this.Display_PassPort = event.target.result;
        }
      }
      var file = event.target.files[0];
      this.Display_PassPort = file.name;
      this.fileExtension = this.Display_PassPort.split('.').pop();
      if (this.fileExtension == "pdf") {
        this.FileExtPassPort = true;
      }
      else {
        this.FileExtPassPort = false;
      }
    }
    if (strName == "DrivingLicence") {
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]); // read file as data url
        reader.onload = (event: any) => { // called once readAsDataURL is completed
          this.Display_DrivingLicence = event.target.result;
        }
      }
      var file = event.target.files[0];
      this.Display_DrivingLicence = file.name;
      this.fileExtension = this.Display_DrivingLicence.split('.').pop();
      if (this.fileExtension == "pdf") {
        this.FileExtDrivingLicence = true;
      }
      else {
        this.FileExtDrivingLicence = false;
      }
    }
    if (strName == "ProofofAddress") {
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]); // read file as data url
        reader.onload = (event: any) => { // called once readAsDataURL is completed
          this.Display_ProofAddress = event.target.result;
        }
      }
      var file = event.target.files[0];
      this.Display_ProofAddress = file.name;
      this.fileExtension = this.Display_ProofAddress.split('.').pop();
      if (this.fileExtension == "pdf") {
        this.FileExtProofAddress = true;
      }
      else {
        this.FileExtProofAddress = false;
      }
    }
    if (strName == "DBSCertificate") {
      if (event.target.files && event.target.files[0]) {
        var reader = new FileReader();
        reader.readAsDataURL(event.target.files[0]); // read file as data url
        reader.onload = (event: any) => { // called once readAsDataURL is completed
          this.Display_DBSCertificate = event.target.result;
        }
      }
      var file = event.target.files[0];
      this.Display_DBSCertificate = file.name;
      this.fileExtension = this.Display_DBSCertificate.split('.').pop();
      if (this.fileExtension == "pdf") {
        this.FileExtDBSCertificate = true;
      }
      else {
        this.FileExtDBSCertificate = false;
      }
    }
  }

  /*- checks if word exists in array -*/
  isInArray(array, word) {
    return array.indexOf(word.toLowerCase()) > -1;
  }

  /*- Required Fields -*/
  get f() { return this.doctorprofileform.controls; }
  buildForm() {
    this.doctorprofileform = this.formBuilder.group({
      FirstName: ['', Validators.required],
      LastName: ['', Validators.required],
      dateofbirth: ['', Validators.required],
      EmailAddress: ['', [Validators.required, Validators.pattern(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      PhoneNumber: ['', [Validators.required, Validators.minLength(10)]],
      Gender: ['', Validators.required],
      Category: ['', Validators.required],
      AboutMe: ['', Validators.required],
      BasicPrice: ['', Validators.required],
      PlaceName: ['', Validators.required],
      ProfilePick: ['', Validators.required],
      CV: ['', Validators.required],
      PassPort: ['', Validators.required],
      DrivingLicence: ['', Validators.required],
      ProofofAddress: ['', Validators.required],
      DBSCertificate: ['', Validators.required],
      DBS: ['', Validators.required],

    });
  }
  /*- Only Number Enter -*/
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}

