// import { Component, OnInit } from '@angular/core';

// @Component({
//   selector: 'app-bookhomevisitproceedtopay',
//   templateUrl: './bookhomevisitproceedtopay.component.html',
//   styleUrls: ['./bookhomevisitproceedtopay.component.css']
// })
// export class BookhomevisitproceedtopayComponent implements OnInit {

//   constructor() { }

//   ngOnInit(): void {
//   }

// }
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IPayPalConfig, ICreateOrderRequest, IPurchaseUnit } from 'ngx-paypal';

@Component({
  selector: 'app-bookhomevisitproceedtopay',
  templateUrl: './bookhomevisitproceedtopay.component.html',
  styleUrls: ['./bookhomevisitproceedtopay.component.css']
})
export class BookhomevisitproceedtopayComponent implements OnInit {
  CheckoutForm: FormGroup;
  public payPalConfig?: IPayPalConfig;

  constructor(private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.buildForm();
    this.initConfig();
  }

  onSubmit() {
    // stop here if form is invalid
    if (this.CheckoutForm.invalid) {
      return;
    }
  }

  get f() { return this.CheckoutForm.controls; }
  buildForm() {
    this.CheckoutForm = this.formBuilder.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      EmailAddress: ['', [Validators.required, Validators.pattern(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      PhoneNumber: ['', [Validators.required, Validators.minLength(10)]],
      DateofBirth: ['', Validators.required],
      Address: ['', Validators.required],
      ReadAndAccept: ['', Validators.required],
    }
    );
  }
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  // payPal Payment Method

  private initConfig(): void {
    this.payPalConfig = {
      currency: 'GBP',
      clientId: 'sb',
      style: {
        label: 'paypal',
        layout: 'horizontal'
      },

      onApprove: (data, actions) => {
        console.log('onApprove - transaction was approved, but not authorized', data, actions);
      },

      onCancel: (data, actions) => {
        console.log('OnCancel', data, actions);
      },

      onError: err => {
        console.log('OnError', err);
      },

      onClick: () => {
        console.log('onClick');
      },
    };
  }
}
