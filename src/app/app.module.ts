import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './common-comp/header/header.component';
import { FooterComponent } from './common-comp/footer/footer.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ForServiceProviderComponent } from './for-service-provider/for-service-provider.component';
import { ForServiceUserComponent } from './for-service-user/for-service-user.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { TermsConditionsComponent } from './terms-conditions/terms-conditions.component';
import { PrivacyPolicyComponent } from './privacy-policy/privacy-policy.component';
import { CookiesPolicyComponent } from './cookies-policy/cookies-policy.component';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NewsLatterComponent } from './news-latter/news-latter.component';
import { NgxUiLoaderModule,NgxUiLoaderConfig,} from "ngx-ui-loader";
const ngxUiLoaderConfig: NgxUiLoaderConfig = {
  bgsColor: "#F00",
  bgsOpacity: 0.5,
  bgsPosition: "bottom-left",
  bgsSize: 60,
  bgsType: "ball-spin-clockwise",
  blur: 3,
  delay: 0,
  fastFadeOut: true,
  fgsColor: "#F00",
  fgsPosition: "center-center",
  fgsSize: 30,
  fgsType: "circle",
  gap: 24,
  logoPosition: "top-left",
  logoSize: 120,
  logoUrl: "",
  masterLoaderId: "",
  overlayBorderRadius: "0",
  overlayColor: "rgba(40, 40, 40, 0.8)",
  pbColor: "#F00",
  pbDirection: "ltr",
  pbThickness: 3,
  hasProgressBar: true,
  text: "",
  textColor: "#FFFFFF",
  textPosition: "center-center",
  maxTime: -1,
  minTime: 300
};


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    ForServiceProviderComponent,
    ForServiceUserComponent,
    ContactUsComponent,
    TermsConditionsComponent,
    PrivacyPolicyComponent,
    CookiesPolicyComponent,
    HeaderComponent,
    FooterComponent,
    PagenotfoundComponent,
    NewsLatterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgxUiLoaderModule.forRoot(ngxUiLoaderConfig)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
