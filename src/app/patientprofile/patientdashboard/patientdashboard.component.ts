import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as $ from 'jquery';

@Component({
  selector: 'app-patientdashboard',
  templateUrl: './patientdashboard.component.html',
  styleUrls: ['./patientdashboard.component.css']
})
export class PatientdashboardComponent implements OnInit {

  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
    $(document).ready(function () {
      $('html, body').animate(
        {
          scrollTop: 1,
        },
        600
      );
      return false;
    });
  }

  ngOnDestroy(): void {
    $(document).ready(function () {  // Circle Progress JS 
      $('html, body').animate(
        {
          scrollTop: 0,
        },
        600
      );
      return false;
    });
  }

  open(content) {
    this.modalService.open(content)
  }

}
