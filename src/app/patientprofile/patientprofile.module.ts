import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PatientprofileRoutingModule } from './patientprofile-routing.module';
import { PatientprofileComponent } from './patientprofile.component';
import { PatientdashboardComponent } from './patientdashboard/patientdashboard.component';
import { PatientprofilesettingComponent } from './patientprofilesetting/patientprofilesetting.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UiSwitchModule } from 'ngx-ui-switch';


@NgModule({
  declarations: [PatientprofileComponent, PatientdashboardComponent, PatientprofilesettingComponent],
  imports: [
    CommonModule,
    PatientprofileRoutingModule,
    FormsModule, ReactiveFormsModule,
    UiSwitchModule.forRoot({
      size: 'small',
      color: 'rgb(0, 189, 99)',
    })
  ]
})
export class PatientprofileModule { }
