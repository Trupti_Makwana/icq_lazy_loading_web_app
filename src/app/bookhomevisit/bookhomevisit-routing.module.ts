import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BookhomevisitComponent } from './bookhomevisit.component';
import { BookhomevisitdoctorlistComponent } from './bookhomevisitdoctorlist/bookhomevisitdoctorlist.component';
import { BookhomevisitdoctorviewprofileComponent } from './bookhomevisitdoctorviewprofile/bookhomevisitdoctorviewprofile.component';
import { BookhomevisitdoctorbookappointmentComponent } from './bookhomevisitdoctorbookappointment/bookhomevisitdoctorbookappointment.component';
import { BookhomevisitproceedtopayComponent } from './bookhomevisitproceedtopay/bookhomevisitproceedtopay.component';
import { BookhomevisitbookedsuccessfullyComponent } from './bookhomevisitbookedsuccessfully/bookhomevisitbookedsuccessfully.component';


const routes: Routes = [
  {
    path: '', component: BookhomevisitComponent,
    children: [
      { path: 'service-provider-list', component: BookhomevisitdoctorlistComponent },
      { path: 'service-provider-viewprofile', component: BookhomevisitdoctorviewprofileComponent },
      { path: 'service-provider-bookappointment', component: BookhomevisitdoctorbookappointmentComponent },
      { path: 'home-visit-proceedtopay', component: BookhomevisitproceedtopayComponent },
      { path: 'booked-successfully', component: BookhomevisitbookedsuccessfullyComponent },
    ]
  }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BookhomevisitRoutingModule { }
