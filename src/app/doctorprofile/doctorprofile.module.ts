import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DoctorprofileRoutingModule } from './doctorprofile-routing.module';
import { DoctorprofileComponent } from './doctorprofile.component';
import { DoctordashboardComponent } from './doctordashboard/doctordashboard.component';
import { DoctorprofilesettingComponent } from './doctorprofilesetting/doctorprofilesetting.component';
import { DoctorreviewComponent } from './doctorreview/doctorreview.component';
import { DoctorhomevisitComponent } from './doctorhomevisit/doctorhomevisit.component';
import { DoctorchangepasswordComponent } from './doctorchangepassword/doctorchangepassword.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UiSwitchModule } from 'ngx-ui-switch';

@NgModule({
  declarations: [DoctorprofileComponent, DoctordashboardComponent, DoctorprofilesettingComponent, DoctorreviewComponent, DoctorhomevisitComponent, DoctorchangepasswordComponent],
  imports: [
    CommonModule,
    DoctorprofileRoutingModule,
    FormsModule, ReactiveFormsModule,
    UiSwitchModule.forRoot({
      size: 'small',
      color: 'rgb(0, 189, 99)',
      switchColor: '#f5f5f5',
      defaultBgColor: '#f5f5f5',
      defaultBoColor: '#476EFF',
    })
  ]
})
export class DoctorprofileModule { }
