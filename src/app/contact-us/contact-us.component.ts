import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';


@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  contactusForm: FormGroup;
  rout: string;

  constructor(public router: Router, private formBuilder: FormBuilder) {
    this.rout = router.url;
  }

  ngOnInit(): void {
    this.buildForm();
  }

  onSubmit() {
    // stop here if form is invalid
    if (this.contactusForm.invalid) {
      return;
    }
  }

  // convenience getter for easy access to form fields
  get f() { return this.contactusForm.controls; }
  buildForm() {
    this.contactusForm = this.formBuilder.group({
      Name: ['', Validators.required],
      EmailAddress: ['', [Validators.required, Validators.pattern(/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]],
      Message: ['', Validators.required],
    }
    );
  }
}
